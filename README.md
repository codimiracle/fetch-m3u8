# fetch-m3u8

### 介绍

通过 m3u8 连接获取整个视频，该工具会先下载好分片，并通过 ffmpeg 对视频进行转换得到合并的视频。

### 安装教程

若不需要修改代码，请直接使用 release 中提供的包。

1.  安装 rust
2.  [下载 ffmpeg essentials 版本，放在 third-party 目录下](./third-party/README.md)
3.  cargo build
4.  cargo run <url> <directory>

### 使用说明

该工具比较简易，只是，只需要记住下面的命令行即可，\<url\> 为 m3u8 视频的链接，\<directory\> 为暂时存放视频片段的路径。

```
fetch-m3u8 <url> <directory>
```

### 参与贡献

1.  Fork 本仓库
2.  新建 feature_xxx 分支
3.  提交代码
4.  新建 Pull Request
