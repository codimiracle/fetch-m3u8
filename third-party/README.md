### 第三方应用

[FFMPEG 下载链接](https://www.gyan.dev/ffmpeg/builds/)
![](./snapshot.png)
下载 ffmpeg-release-essentials.7z 即可，
将 ffmpeg 程序放在这个目录下，最终结构如下：

```
thrid-party
--ffmpeg
  |-bin
  |-doc
  --presets
....
```
